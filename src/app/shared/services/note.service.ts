import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Note, Type } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor(private http:HttpClient) { }
  getTypes():Promise<Type[]>{
    return this.http.get<Type[]>(`${environment.apiUrl}/types`).toPromise();
  }
  getNotes():Promise<Note[]>{
    return this.http.get<Note[]>(`${environment.apiUrl}/notes`).toPromise();
  }
  getNote(id:number):Promise<Note>{
   return this.http.get<Note>(`${environment.apiUrl}/notes/${id}`).toPromise();
  }
  postNote(data:Note):Promise<Note>{
    return this.http.post<Note>(`${environment.apiUrl}/notes/`, data).toPromise();
   } 
   putNote(data:Note,id:any):Promise<Note>{
    return this.http.put<Note>(`${environment.apiUrl}/notes/${id}`, data).toPromise();
  }
  deleteNote(id:number):Promise<Note>{
    return this.http.
    delete<Note>(`${environment.apiUrl}/notes/${id}`)
    .toPromise();
  }
  deleteType(id:number):Promise<Type>{
    return this.http.
    delete<Type>(`${environment.apiUrl}/types/${id}`)
    .toPromise();
  }
  postType(data:Type):Promise<Type>{
    return this.http.post<Type>(`${environment.apiUrl}/types/`, data).toPromise();
   } 
   putType(data:Type,id:any):Promise<Type>{
    return this.http.put<Type>(`${environment.apiUrl}/types/${id}`, data).toPromise();
  }
  }