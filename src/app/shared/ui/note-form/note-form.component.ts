import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Note, Type } from 'src/app/shared/interfaces/interfaces';

@Component({
  selector: 'app-note-form',
  templateUrl: './note-form.component.html',
  styleUrls: ['./note-form.component.css']
})
export class NoteFormComponent implements OnInit {
  @Output() onSave = new EventEmitter<Note>();
  noteForm!: FormGroup;
  @Input() showOther!:boolean;
  @Input() types!:Type[];
  constructor(private fb: FormBuilder) { }
  index!:number;
  ngOnInit(): void {
    const controls = {
      name: [null, [Validators.required, Validators.maxLength(100)]],
      noteText: [null, [Validators.required, Validators.maxLength(1000)]],
      time: [null],
      // noteType: [null, [Validators.required]],
      noteType: [0,[Validators.required]],
      showed: [true],
      }
    this.noteForm = this.fb.group(controls);
  };
  save() {
    this.noteForm.value.showed =true
    console.log(this.noteForm.value.noteType)
    
   
    this.noteForm.value.time = (new Date).toLocaleString('ru-RU')
    const note = this.noteForm.value;
    console.log(note)
    this.onSave.emit(note);
    this.noteForm.reset();
   
  }


}