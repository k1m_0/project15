import { HttpClient } from '@angular/common/http';
import { EventEmitter, Output } from '@angular/core';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Note, Type } from 'src/app/shared/interfaces/interfaces';
import { NoteService } from '../../services/note.service';

@Component({
  selector: 'app-note-item',
  templateUrl: './note-item.component.html',
  styleUrls: ['./note-item.component.css']
})
export class NoteItemComponent implements OnInit {
  @Input() note!:Note;
  // @Input() note!:Note;
  @Output() onDelete = new EventEmitter<number>();
  @Output() onSave = new EventEmitter<Note>();
  @Output() onHide = new EventEmitter<Note>();
  @Output() onShowForm = new EventEmitter<boolean>();
   types!:Type[];
  editMode = false;
  noteForm!: FormGroup;
    constructor(private fb:FormBuilder, private noteService: NoteService) { }
  // typer:any;
  


    ngOnInit(): void {
      const controls = {
        name: [null, [Validators.required, Validators.maxLength(100)]],
        noteText: [null, [Validators.required, Validators.maxLength(1000)]],
      noteType:[null, []],
      }
      this.getTypes();
      this.noteForm = this.fb.group(controls);
      
      if(this.note){
        this.noteForm.patchValue(this.note); 
         
         
      }
    }
    async getTypes() {
      try {
        this.types = (await this.
          noteService.getTypes()) || []

         
        
        } catch (error) {
        console.log(error)
      }
    }
    getTypeName(id:number){
      // this.typer = this.types.find((x)=>x.id == this.note.noteType)
      let type =this.types?.find(x=>x.id == id);
      return type?.name
    }

    editing(){
      this.editMode = true

   
      // this.onHide.emit(this.note)
    }
   
  
    delete(){
      this.onDelete.emit(this.note.id)
    }
  
    save(){
      this.noteForm.value.showed = true
      this.noteForm.value.updateTime = (new Date).toLocaleString('ru-RU')
      this.noteForm.value.time = this.note.time
      const note = this.noteForm.value
      console.log(note)
      this.onSave.emit(note)
      this.editMode = false
      this.onShowForm.emit(false)
    }
  }
  