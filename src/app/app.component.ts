import { Component } from '@angular/core';
import { Note, Type } from './shared/interfaces/interfaces';
import { NoteService } from './shared/services/note.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'project15';
  notes!: Note[];
  noteChanged!: Note;
  types!: Type[];
  showOther = true;
  hideForm=false;
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.getData()
    this.getTypes()
  }
  constructor(private noteService: NoteService) {

  }
  async getTypes() {
    try {
      this.types = (await this.
        noteService.getTypes()) || []
    } catch (error) {
      console.log(error)
    }
  }
  async getData() {
    try {
      this.notes = (await this.
        noteService.getNotes()) || []
    } catch (error) {
      console.log(error)
    }
  }

  async save(note: Note) {
    console.log(note)
    try {
      await this.noteService.postNote(note)
      this.getData();
    } catch (error) {
      console.log(error)
    }
  }
  async delete(id: number) {
    console.log(id)
    try {
      await this.noteService.deleteNote(id)
      this.getData();
    } catch (error) {
      console.log(error)
    }
  }
  async deleteT(id: any) {
    console.log(id)
    try {
      await this.noteService.deleteType(id)
      this.getTypes();
    } catch (error) {
      console.log(error)
    }
  }

  async postType(type:Type){
    
    try {
      await this.noteService.postType(type)
      this.getTypes();
    } catch (error) {
      console.log(error)
    }
  }


  ShowForm(f: boolean) {
    this.hideForm= f;
  }

  hideAll(note: Note){
    for(let nH of this.notes){
      nH.showed=false
    }
    this.hideForm=true;
    note.showed=true
  }

  editing(note: Note) {

    console.log(this.showOther)
    this.noteChanged = note;
    console.log(note)
  }
  async edit(note: Note, id: any) {
    // console.log(id)
    try {
      console.log('asa')
      console.log(note)
      await this.noteService.putNote(note, id)


      this.getData();
    } catch (error) {
      console.log(error)
    }
  }
  async putT(type: Type,id:any) {
    // console.log(id)
    try {
     
      await this.noteService.putType(type,id)


      this.getTypes();
    } catch (error) {
      console.log(error)
    }
  }


  showTM=false
  ShowTypeMenu(){

    for(let nH of this.notes){
      nH.showed=false
    }
    this.hideForm=true;
    this.showTM=true
  }
  showMM(){

    for(let nH of this.notes){
      nH.showed=true
    }
    this.hideForm=false;
    this.showTM=false
  }
}
