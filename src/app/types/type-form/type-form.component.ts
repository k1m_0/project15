import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Type } from 'src/app/shared/interfaces/interfaces';

@Component({
  selector: 'app-type-form',
  templateUrl: './type-form.component.html',
  styleUrls: ['./type-form.component.css']
})
export class TypeFormComponent implements OnInit {
  @Output() onDelT = new EventEmitter<number>();
  @Output() onPutT = new EventEmitter<Type>();
  @Output() onSave = new EventEmitter<Type>();
  @Input() type!:Type;
  constructor(private fb:FormBuilder) { }
  editMode = false;
  typeForm!: FormGroup;
  id:any;
  ngOnInit(): void {
    const controls = {
      name: [null, [Validators.required, Validators.maxLength(100)]],
    }
    this.typeForm = this.fb.group(controls);
    if(this.type){
      this.typeForm.patchValue(this.type); 
      this.id = this.type.id;
    }
  }
  AddType(){
    
  }
  DelType(id:any){
   
      this.onDelT.emit(id)
   
  }
  editType(){
   
    this.editMode = true;
   
  }
  save(){
    const type = this.typeForm.value
    console.log(type)
    this.onSave.emit(type)
    this.editMode = false
  }
}
