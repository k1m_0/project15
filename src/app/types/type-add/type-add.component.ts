import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Type } from 'src/app/shared/interfaces/interfaces';

@Component({
  selector: 'app-type-add',
  templateUrl: './type-add.component.html',
  styleUrls: ['./type-add.component.css']
})
export class TypeAddComponent implements OnInit {

  
    @Output() onSave = new EventEmitter<Type>();
    @Output() retMM = new EventEmitter();
    typeForm!: FormGroup;
   
    @Input() types!:Type[];
    constructor(private fb: FormBuilder) { }
  
    ngOnInit(): void {
      const controls = {
        id: [null],
        name: [null, [Validators.required, Validators.maxLength(100)]],
        }
      this.typeForm = this.fb.group(controls);
    };
    save() {
      this.typeForm.value.time = (new Date).toLocaleString('ru-RU')
      const type = this.typeForm.value;
      console.log(type)
      this.onSave.emit(type);
      this.typeForm.reset();
     
    }
  
    returnMM(){
      console.log('asdasdasdasdasd')
      this.retMM.emit();
    }
  }