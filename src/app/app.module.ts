import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NoteFormComponent } from './shared/ui/note-form/note-form.component';
import { NoteItemComponent } from './shared/ui/note-item/note-item.component';
import { TypeFormComponent } from './types/type-form/type-form.component';
import { TypeAddComponent } from './types/type-add/type-add.component';

@NgModule({
  declarations: [
    AppComponent,
    NoteFormComponent,
    NoteItemComponent,
    TypeFormComponent,
    TypeAddComponent
  ],
  imports: [
    BrowserModule, HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
